var twitterClient = require('./verifyTwitter');
// var getImage = require('./imageSearch');
var deleteImages = require('./deleteFiles');
// var db = require('./low_db');

var isDev = false;

/**
 * Returns a random integer between min (inclusive) and max (inclusive)
 */
function getRandom(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}


function tweet ( status, next ) {

	next = next || function () {};

	twitterClient.post( 'statuses/update', status, function ( error, tweet, response ) {
		if ( !error ) {
			
			console.log('SUCCESS');
			console.log(tweet);

			// Run the callback
			next();
					
		} else {
			console.log(error);
		}
	});
}

function tweetWithImage ( message, imageURL ) {

	console.log('message: '+message);
	console.log('imageURL: '+imageURL);
	
	// Load the image data
	var imageData = require('fs').readFileSync( imageURL );

	twitterClient.post( 'media/upload',
		
		{ media: imageData },
		
		function ( error, media, response ) {
		
			if ( !error ) {
				
				console.log('SUCCESS');
				console.log( message );
				console.log( media );

				// Build the tweet
				var status = {
					status: message,
					media_ids: media.media_id_string
				};

				// Tweet it
				tweet( status, function () {
					var imageFolder = imageURL.split('/')[0];
					imageFolder = imageFolder + '/';
					deleteImages( imageFolder );
					
				});

			} else {
				console.log('*** ERROR ***');
				console.log(error);
			}
	});	
}

function postTweet ( withImage ) {
	
	var tweet = 'Message for twitter';
	var post = { status: tweet };

	console.log( 'TWEET: ' + tweet );

	if ( isDev ) {
		return;
	}
	
	// Posting with an image
	if ( withImage ) {
		var imageURL = 'something.jpg';
		tweetWithImage( tweet, imageURL );
	// Posting with just text
	} else {
		tweet( post );
	}
	
}

module.exports = postTweet;