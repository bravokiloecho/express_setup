var fs = require('fs');

function deleteFiles ( path, next ) {
	fs.readdirSync( path ).forEach(function ( filename ) {
				
		// Rules for files...
		// Don't delete hidden files (eg the .gitkeep)
		if ( filename[0] === '.' ) { return; }

		fs.unlinkSync( path + filename );
	});
}

module.exports = deleteFiles;