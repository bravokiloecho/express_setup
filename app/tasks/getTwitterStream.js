var twitterClient = require('./verifyTwitter');

// See here for list of params:
// https://dev.twitter.com/rest/reference/get/statuses/user_timeline

function getStream ( params, next ) {

	// See below for params
	// var params = {
	// 	screen_name: userName,
	// 	count: count,
	// 	exclude_replies: excludeReplies ? excludeReplies : false,
	// 	include_rts: includeRTS ? includeRTS : false,
	// };

	twitterClient.get( 'statuses/user_timeline', params, function ( error, tweets, response ) {
		if ( !error ) {
			// console.log('tweet');
			// console.log(tweet);
			next( tweets );
					
		} else {
			console.log('*** ERROR GETTING STREAM ***');
			console.log(error);
		}
	});
}


module.exports = getStream;