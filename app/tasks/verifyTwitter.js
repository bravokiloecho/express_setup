// Using: https://github.com/istrategylabs/node-twitter
var Twitter = require('twitter');
var env = require('node-env-file');
env('../.env_secret');

var twitterClient = new Twitter ({
  consumer_key: process.env.CONSUMERKEY,
  consumer_secret: process.env.CONSUMERSECRET,
  access_token_key: process.env.ACCESSTOKENKEY,
  access_token_secret: process.env.ACCESSTOKENSECRET
});

module.exports = twitterClient;