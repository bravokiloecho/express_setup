var low = require('lowdb');
var dbPath = 'low_db/numbers.json';


var db = low( dbPath , {
  autosave: true, // automatically save database on change (default: true)
  async: true     // asynchronous write (default: true)
});

function addToDB ( entry, section ) {
	db( section ).push({ title: entry });
}

function textExistence ( query, section ) {

	console.log('searching DB for ' + query );
	console.log('SIZE OF DB ' + db( section ).size() );
	
	// var result = db( section ).includes( query );
	var result = db( section ).find( { title: query } );
	console.log('Result of DB search: ' + result );
	
	return result;
}

module.exports = {
	addToDB: addToDB,
	textExistence: textExistence
};