var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var app = module.exports = express();

var staticPath = path.join(__dirname, 'public');


// DEFINE SOME APP LOCALS FROM .ENV FILE
var env = require('node-env-file');
var localSettingsFile = '.locals_local';
var liveSettingsFile = '.locals_live';

// META INFO
var meta = {
  title: 're:Masters',
  description: 'Find your muses the hard way',
  author: 'ben@elwyn.co'
};


// LOAD LOCAL SETTINGS FILE
env( path.join(__dirname, '..') + '/' + localSettingsFile );
// If production also load production settings file
if (app.settings.env !== 'development') {
    env( path.join(__dirname, '..') + '/' + liveSettingsFile, { overwrite: true } );
}
app.locals = {
  title: meta.title,
  description: meta.description,
  author: meta.author,
  url: process.env.url,
  assetPath: process.env.assetPath,
  pretty: true,
  settings: {
    env: app.settings.env
  }
};


// BROWSER SYNC
if (app.get('env') === 'development') {
  var injectFiles =   [
    staticPath + "/css/mouse/*.css",
    staticPath + "/gfx/*",
    staticPath + "/gfx/*/*",
    staticPath + "/images/*",
  ];

  var browserSync = require('browser-sync');
  var bs = browserSync({
    port: 3000,
    logSnippet: true,
    files: injectFiles
  });


  app.use(require('connect-browser-sync')(bs));
}


// VIEW ENGINE SETUP
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use( '/static', express.static(path.join(__dirname, 'public')));

// Load routes
require(path.join(__dirname, 'routes', 'routes.js'))( app );

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('errors/error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});
