var CronJob = require('cron').CronJob;
var moment = require('moment');
var postTweet = require( './tasks/postTweet' );
var app = require('./app');
var isDev = app.settings.env === 'development' ? true : false;

console.log('is Dev?: '+ isDev);


module.exports = function ( postTweet ) {

	function getRandom(min, max) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	function normalizeTime ( value, type ) {
		
		var maxValue = 59;

		if ( type === 'hours' ) {
			maxValue = 23;
		}

		if ( value > maxValue ) {
			value = value - maxValue;
		}

		return value;
	}

	// Defines a Cron pattern based on a random number 
	// of minutes and hours from the current time
	function getDelay () {
		
		// Define range of random values
		var seconds = { min: 1, max: 59 };
		var mins = { min: 1, max: 59 };
		var hours = { min: 5, max: 24 };

		// Get time value now
		var secondsNow = parseInt( moment().format("ss") );
		var minutesNow = parseInt( moment().format("mm") );
		var hoursNow = parseInt( moment().format("H") );

		// Define length of time to wait
		var secondsToWait = getRandom( seconds.min, seconds.max );
		var minutesToWait = getRandom( mins.min, mins.max );
		var hoursToWait = getRandom( hours.min, hours.max );

		// Calculate cron values
		var secondsValue = secondsNow + secondsToWait;
		var minutesValue = minutesNow + minutesToWait;
		var hoursValue = hoursNow + hoursToWait;

		// Sanitize for clockface
		secondsValue = normalizeTime( secondsValue );
		minutesValue = normalizeTime( minutesValue );
		hoursValue = normalizeTime( hoursValue, 'hours' );

		// Create CRON pattern
		
		// Runs off minutes and seconds
		// var pattern = secondsValue + ' ' + minutesValue + ' * * * *';

		// Runs off Hours and minutes and seconds
		var pattern = secondsValue + ' ' + minutesValue + ' ' + hoursValue + ' * * *';
		
		return pattern;
	}



	function testCronPattern ( pattern ) {
		
		console.log('Testing: ' + pattern);
		
		try {
		    new CronJob( pattern , function() {
		        console.log('this should not be printed');
		    });
		} catch(ex) {
		    console.log("cron pattern not valid");
		}
	}

	function runJob ( initial ) {
		
		var cronPattern;

		cronPattern = getDelay();
		
		// Test the patern (optional)
		if ( isDev ) {
			testCronPattern( cronPattern );
		}

		console.log('Run cron task with pattern: ' + cronPattern );
		
		var job = new CronJob({ // run at 10:30 am every day
			
			cronTime: cronPattern,
			onTick: function () {
				
				// Log the time
				var time = moment().format("H:mm:ss");
				console.log('cron log @ ' + time);

				// RUN THE TASK
				
				// Stop the job and begin again
				this.stop();
				runJob();
			},
			onComplete: function () {
				console.log('STOP');
			},
			start: false

		}).start();
	}

	// BEGIN
	runJob( true );

};


