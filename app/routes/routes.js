module.exports = function ( app ) {

	// Load twitter functions
	var postTweet = require( '../tasks/postTweet' );
	var getStream = require( '../tasks/getTwitterStream' );

	// HOME
	app.route('/')
		.get( function (req, res) {
			res.render('index',{
	  
			});
		});

	// Load up twitter stream
	app.route('/api/v1/getStream')
	  
		.post(function (req, res) {

			var user = req.body.user;
			var count = req.body.count;
			var lastTweetID = parseInt( req.body.tweetID );
			var lastNumber = req.body.lastNumber;
			var page = req.body.page;

			var params = {
			  user_id: user,
			  count: count
			};

			console.log('LAST TWEET ID');
			console.log( lastTweetID);

			console.log('LAST TWEET Number');
			console.log( lastNumber );

			if ( lastTweetID !== 0 ) {
				console.log('REQUEST LAST ID: ' + lastTweetID);
				params.max_id = lastTweetID;
			}

			getStream( params, function ( tweets ) {
				var response = tweets;
				res.send( response );
			});
	  });

};
