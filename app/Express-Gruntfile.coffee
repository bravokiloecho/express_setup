module.exports = (grunt) ->
  
  # Project configuration.
  grunt.initConfig
    pkg: grunt.file.readJSON("package.json")
    
    # DEFINE PATH TO ASSET FOLDER
    # leave blank if in same folder as gruntfile
    express_path: './' 
    asset_path: './devAssets/'
    text_path: './text/'
    public_path: './public/'
    views_path: './views/'
    bucket_path: 'FOLDER/'
    port: '3000'



    watch:
      sass:
        files: [
          "<%= asset_path %>sass/*"
          "<%= asset_path %>sass/*/*"
        ]
        tasks: ["compass:dev"]

      coffee:
        files: ["<%= asset_path %>coffee/*"]
        tasks: ["coffee"]
        atBegin: true


      # refresh page when raw assests change
      assets:
        files: [
          "<%= public_path %>js/mouse/*"
          "<%= views_path %>*"
          "<%= views_path %>*/*"
          "<%= text_path %>*.mdown"
        ]
        options:
          livereload: true

      plugins:
        files: ["<%= asset_path %>plugins/*"]
        tasks: ["uglify:plugins"]



    uglify:
      plugins:
        options:
          mangle: true
          beautify: false
          compress: true

        files:
          "<%= public_path %>js/plugins.js": ["<%= asset_path %>plugins/*.js"]

      
      # combines plugins and scripts
      combine_all:
        options:
          mangle: true
          beautify: false
          compress:
            drop_console: true
        files:
          "<%= public_path %>js/min/scripts.js": [
            "<%= public_path %>js/plugins.js"
            "<%= public_path %>js/mouse/scripts.js"
          ]

    imagemin: # Task
      dynamic: # Another target
        options:
          cache: false
          optimizationLevel: 2

        files: [
          expand: true # Enable dynamic expansion
          cwd: "<%= asset_path %>gfx/" # Src matches are relative to this path
          src: ["*.{png,jpg,gif}"] # Actual patterns to match
          dest: "<%= public_path %>gfx/min/" # Destination path prefix
        ]

    svgmin:
      options:
        plugins: [
          {
            removeViewBox: false
          }, {
            removeUselessStrokeAndFill: true
          }
        ]
      dist:
        files: [
          expand: true # Enable dynamic expansion
          cwd: "<%= asset_path %>svg/" # Src matches are relative to this path
          src: ["*.svg","*/*.svg"] # Actual patterns to match
          dest: "<%= public_path %>gfx/" # Destination path prefix
        ]

    compass:
      dev:
        options:
          sassDir: "<%= asset_path %>sass"
          cssDir: "<%= public_path %>css/mouse"
          # outputStyle: "nested"
          fontsPath: "Fonts"
          force: true
          sourcemap: true

      dist:
        options:
          sassDir: "<%= asset_path %>sass"
          cssDir: "<%= public_path %>css/min"
          outputStyle: "compressed"
          fontsPath: "<%= public_path %>Fonts"
          force: true

    coffee:
      compileBare:
        options:
          bare: true
          join: true

        files:
          "<%= public_path %>js/mouse/scripts.js": ["<%= asset_path %>coffee/*.coffee"]

    # Deploy to amazon S3
    # Readme: https://github.com/jpillora/grunt-aws
    aws: grunt.file.readJSON '../aws/credentials.json'
    s3:
      options:
        accessKeyId: "<%= aws.accessKeyId %>"
        secretAccessKey: "<%= aws.secretAccessKey %>"
        bucket: "assets.elwyn.co"
        region: 'eu-west-1'
        gzip: true
        gzipExclude: ['.jpg', '.jpeg', '.png', '.woff', '.woff2', '.eot', '.ttf']
      public:
        cwd: "<%= public_path %>"
        src: [
          "css/**/*"
          "js/**/*"
          "gfx/**/*"
          "Fonts/**/*"
        ]
        dest: "<%= bucket_path %>"


    shell:
      start: 'npm start'
      deploy: 'git push web'

    
  # Load grunt plugins.
  grunt.loadNpmTasks "grunt-contrib-compass"
  grunt.loadNpmTasks "grunt-contrib-watch"
  grunt.loadNpmTasks "grunt-contrib-uglify"
  grunt.loadNpmTasks "grunt-contrib-coffee"
  grunt.loadNpmTasks "grunt-svgmin"
  grunt.loadNpmTasks "grunt-aws"
  grunt.loadNpmTasks "grunt-shell"


  
  #register tasks
  
  #watch with css inject
  grunt.registerTask "default", [
    "shell:start"
  ]

  grunt.registerTask "w", [
    "watch"
  ]
  
  # compile all files that need compiling
  grunt.registerTask "c", [
    "compass"
    "coffee"
    "uglify:plugins"
    "uglify:combine_all"
  ]

  # deploy by compiling assets
  # and uploading to S3
  grunt.registerTask "deploy", [
    "c"
    "s3"
    "shell:deploy"
  ]
  
  grunt.registerTask "coff", ["coffee"]
  grunt.registerTask "js", ["uglify:js_scripts"]
  grunt.registerTask "sass", ["compass"]
  grunt.registerTask "ass", ["assemble"]
  grunt.registerTask "plug", ["uglify:plugins"]
  grunt.registerTask "min", ["imagemin"]
  grunt.registerTask "svg", ["svgmin"]
