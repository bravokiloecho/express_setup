#!/usr/bin/env bash

# Set the environment variable
export NODE_ENV="production"
# Set the port
export PORT=XXXXXX
# Cd into where Grunt is
cd /home/USERNAME/webapps/APPNAME/app/
# Read the .bash_profile to get paths for gems
. /home/USERNAME/.bash_profile
procs=$(forever list | grep -F /home/USERNAME/webapps/APPNAME/app/bin/www)
if [ -z "$procs" ]; then
    forever start -a -o /home/USERNAME/webapps/APPNAME/out.log -e /home/USERNAME/webapps/APPNAME/error.log --uid "_PROCESSID_" /home/USERNAME/webapps/APPNAME/app/bin/www
fi